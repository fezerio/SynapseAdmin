<?php
class QuestionModel
{
    public $id;
    public $chapter_id;
    public $question;
    public $option_a;
    public $option_b;
    public $option_c;
    public $option_d;
    public $answer;
    public $explanation;
    public $created_date;

    function __construct(
        $id,
        $chapter_id,
        $question,
        $option_a,
        $option_b,
        $option_c,
        $option_d,
        $answer,
        $explanation,
        $created_date
    ) {
        $this->$id = $id;
        $this->$created_date = $created_date;
        $this->$chapter_id = $chapter_id;
        $this->$question = $question;
        $this->$option_a = $option_a;
        $this->$option_b = $option_b;
        $this->$option_c = $option_c;
        $this->$option_d = $option_d;
        $this->$answer = $answer;
        $this->$explanation = $explanation;
    }
}
