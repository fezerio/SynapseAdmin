<?php
include('security.php');
include('service/chapterservice.php');
$topic_id = $_GET['topic_id'];
$subject_id = $_GET['subject_id'];
$chapterservice = new ChapterService($connection);

if (isset($_POST['chapteraddbtn'])) {

    $chapter_name = $_POST['chapter'];
    $live_date = new DateTime($_POST['livedate']);
    $date = $live_date->format('Y-m-d H:i:s');

    //test time
    $hrs = $_POST['testtimehr'];
    $mins = $_POST['testtimemin'];
    $test_time = $mins * 60 + $hrs * 3600;



    $result = $chapterservice->addChapter($chapter_name,$topic_id,$subject_id,$test_time,$date);

    if($result){
        // $_SESSION['success'] = $chapter_name . " added!";
        header('Location: chapters.php?topic_id=' . $topic_id . '&subject_id=' . $subject_id . '');
    }else{ 
        $_SESSION['status'] = "Error adding chapter!";
        header('Location: chapters.php?topic_id=' . $topic_id . '&subject_id=' . $subject_id . '');
    }

}

if (isset($_POST['chaptereditbtn'])) {

    $chapter_id = $_POST['chapter_id'];
    $chapter_name = $_POST['chapter_name'];
    $live_date = new DateTime($_POST['livedate']);
    $date = $live_date->format('Y-m-d H:i:s');

    //test time
    $hrs = $_POST['testtimehr'];
    $mins = $_POST['testtimemin'];
    $test_time = $mins * 60 + $hrs * 3600;


    $result = $chapterservice->UpdateChapter($chapter_id,$chapter_name,$topic_id,$subject_id,$test_time,$date);

    if($result){
        // $_SESSION['success'] = $chapter_name . " added!";
        header('Location: chapters.php?topic_id=' . $topic_id . '&subject_id=' . $subject_id . '');
    }else{ 
        $_SESSION['status'] = "Error adding chapter!";
        header('Location: chapters.php?topic_id=' . $topic_id . '&subject_id=' . $subject_id . '');
    }

}
