<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
include('service/userservice.php');
include('service/questionservice.php');
include('service/chapterservice.php');

$userService = new UserService($connection);
?>


<!-- Content Wrapper -->
<div id="content-wrapper" class="d-flex flex-column">

  <!-- Main Content -->
  <div id="content">

    <div class="container-fluid">
      <!-- Content Row -->
      <div class="row">

        <!-- Total Registered User Card -->
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Total Registered Users</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php
                    $row = $userService->getTotalUser();
                    echo '<h1>' . $row . '</h1>'
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-users fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Total Registered Admin Card  -->
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Registered Admin</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php
                    $row = $userService->getTotalAdmin();
                    echo '<h1>' . $row . '</h1>'
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-users-cog fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>

        <!-- Total Chapters Card -->
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Total Chapters </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php
                    $chapterService = new ChapterService($connection);
                    $row = $chapterService->getTotalChapterCount();
                    echo '<h1>' . $row . '</h1>'
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-book fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- Total Questions Card -->
        <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Questions </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php
                    $questionService = new QuestionService($connection);
                    $row = $questionService->getTotalNumberOfQuestions();
                    echo '<h1>' . $row . '</h1>'
                    ?>
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-question-circle fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <!-- Donut Chart -->
      <div class="col-xl-auto col-lg-5 ml-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User Answer Analysis</h6>
          </div>
          <div class="card-body">
            <div class="chart-pie pt-4">
              <canvas id="myPieChart"></canvas>
            </div>
            <hr>
          </div>
        </div>
      </div>
      <div class="col-xl-8 col-lg-4">
        <div class="card shadow mb-4">
          <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">Questions Answered</h6>
          </div>
          <div class="card-body">
            <div class="chart-area">
              <canvas id="chartArea"></canvas>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
</div>



<?php
include('includes/scripts.php');
include('includes/footer.php');
?>