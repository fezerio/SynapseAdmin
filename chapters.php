<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
include('service/subjectservice.php');
require 'service/chapterservice.php';
$chapterservice = new ChapterService($connection);
$subjectservice = new SubjectService($connection);
$topic_id = $_GET['topic_id'];
$subject_id = $_GET['subject_id'];
$subjectname = ($topic_id == 0 && $subject_id == 0) ? 'Daily Dose' : (($topic_id == -1 && $subject_id == -1) ? 'Mock Test' : $subjectservice->getSubjectName($topic_id, $subject_id));
?>

<div class="modal fade" id="addchapter" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Chapter Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="chapter_code.php?topic_id=<?php echo $topic_id; ?>&subject_id=<?php echo $subject_id; ?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">

          <div class="form-group">
            <label> Chapter: </label>
            <input type="text" name="chapter" class="form-control" placeholder="Enter Chapter Name..">
          </div>
          <div class="form-group">
            <label> Enter Live date: </label>
            <input type="date" name="livedate" class="form-control">
          </div>
          <?php
          if ($topic_id == -1 && $subject_id == -1) {
          ?>
            <div class="form-group">
              <label> Enter Time of test: </label>
              <input type="number" name="testtimehr" class="form-control mb-2" placeholder="hrs" min="0" max="23">
              <input type="number" name="testtimemin" class="form-control" placeholder="mins" min="0" max="59">
            </div>
          <?php
          }
          ?>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" name="chapteraddbtn" class="btn btn-primary">Add</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">

    <div class="card-header py-3 mb-4">
      <h6 class="m-0 font-weight-bold text-primary text-uppercase"><?php echo $subjectname; ?> </h6>
    </div>


    <!-- Content Row -->
    <div class="container-fluid">
      <div class="d-sm-flex align-items-center justify-content-between mb-8">
        <h1 class="h3 mb-0 text-gray-800">Chapters</h1>
        <button type="button" data-toggle="modal" data-target="#addchapter" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus-circle fa-sm text-white-50"></i> Add chapter</button>
      </div>

      <!-- Post Slider -->
      <!--         <div class="post-slider">
            <div class="Post-wrapper">
                <div class="post">1</div>

            </div>
        </div> -->
      <!-- End of post slider -->
    </div>


    <div class="card-body">
      <?php
      if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
        echo '<h2>' . $_SESSION['success'] . '</h2>';
        unset($_SESSION['success']);
      }
      if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
        echo '<h2>' . $_SESSION['status'] . '</h2>';
        unset($_SESSION['status']);
      }
      ?>

      <!-- <form action="basic_code.php" method="POST" onkeydown="return event.key != 'Enter';"> -->
      <!-- <div class="form-group">
          <label style="color:black"><b>Subject:</b></label>
          <select aut name="subject" id="subject" class="dropdown-InputBox">
            <option value disabled selected> Select Subject .. </option>
            <?php
            $results = $subjectservice->getSubjects(1);
            foreach ($results as $subject) {
              echo "<option id='" . $subject['id'] . "' value='" . $subject['id'] . "'>" . $subject['name'] . "</option>";
            }
            ?>
          </select>
        </div> -->

      <?php
      $results = $chapterservice->getChapter($topic_id, $subject_id);
      foreach ($results as $chapter) {
        $chapter_id = $chapter["id"];
      ?>

        <div class="card border-left-warning shadow h-100 py-2 mb-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center ">
              <div class="col mr-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php echo ($chapter['name']); ?>
                </div>

                <?php
                $dt = new DateTime($chapter['live_date']);
                $currentDate =  new DateTime("now", new DateTimeZone("UTC"));
                $difference = strtotime($currentDate->format('D M d, Y')) - strtotime($dt->format('D M d, Y'));
                $differenceInDays = (int)($difference / (3600 * 24));

                // echo $currentDate->format('D M d, Y');

                ?>
                <div class="h8 mb-0 font-weight-bold text-red-800">
                  <?php
                  echo ($differenceInDays < 0 ? 'LIVE ON: ' : 'LIVE: ') . ($differenceInDays < 0 ? ((-1 * $differenceInDays) . ' days')  : $dt->format('D M d, Y'))
                  ?>
                </div>
                <div class="h8 mb-0 font-weight-bold text-gray-800">
                  <?php $timeinhr = (int)(($chapter['timer']) / 3600);
                  $timeinmin = round(((abs((($timeinhr * 3600) - (($chapter['timer']))))) / 60), 2);

                  echo ('Test Time: ' . $timeinhr . ' hr ' .  $timeinmin . ' min'); ?>
                </div>
                <a class="h8 mb-0 font-weight-bold text-blue-800" href="questions.php?chapter_id=<?php echo $chapter_id ?>">
                  <div class="h8 mb-0 font-weight-bold text-blue-800">
                    <?php echo "No of questions: " . ($chapter['no_of_questions']); ?>
                  </div>
                </a>
              </div>


              <form action="chapters_edit.php?topic_id=<?php echo $topic_id; ?>&subject_id=<?php echo $subject_id; ?>" method="post">
                <input type="hidden" name="chapter_id" value="<?php echo $chapter['id']; ?>">
                <input type="hidden" name="chapter_name" value="<?php echo $chapter['name']; ?>">
                <input type="hidden" name="timer" value="<?php echo $chapter['timer']; ?>">
                <input type="hidden" name="live_date" value="<?php echo $chapter['live_date']; ?>">
                <button type="submit" name="editchapterbtn" class="h5-inline-block btn btn-info">Edit</button>
              </form>
            </div>
          </div>
        </div>
      <?php
      }


      ?>

      <!-- <div class="form-group">
          <label style="color:black"><b>Chapter:</b></label>
          <select name="chapter" id="chapter" class="dropdown-InputBox">
            <option value="" disabled selected> Select Chapter .. </option>
          </select>
        </div>

        <div class="form-group">
          <label style="color:black"><b>Question:</b></label>
          <input type="text" name="enter_question" class="form-control" placeholder="Enter question here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 1:</b></label>
          <input type="text" name="option_1" class="form-control" placeholder="Enter option 1 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 2:</b></label>
          <input type="text" name="option_2" class="form-control" placeholder="Enter option 2 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 3:</b></label>
          <input type="text" name="option_3" class="form-control" placeholder="Enter option 3 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 4:</b></label>
          <input type="text" name="option_4" class="form-control" placeholder="Enter option 4 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Answer:</b></label>
          <select name="answer" placeholder="Choose correct answer">
            <option value="" disabled selected> Select correct answer... </option>
            <option value="1">Option 1</option>
            <option value="2">Option 2</option>
            <option value="3">Option 3</option>
            <option value="4">Option 4</option>
          </select> -->
      <!-- <label  style="color:black"><b>Answer:</b></label>
                                <input list="options" name="answer" class="form-control" readonly placeholder="Choose the correct answer">
                                <datalist id="options">
                                  <option value="1">
                                  <option value="2">
                                  <option value="3">
                                  <option value="4">
                                </datalist> -->
      <!--  </div>
        <div class="form-group">
          <label style="color:black"><b>Explaination:</b></label>
          <input contenteditable="true" type="text" name="explaination" class="form-control" placeholder="Enter explaination here...">
        </div>

        <a href="register_admin.php" class="btn btn-danger"> CANCEL </a>
        <button type="submit" name="submitbtn" class="btn btn-primary"> Submit </button>

      </form> -->

    </div>
  </div>
</div>

</div>


<?php
include('includes/scripts.php');
?>