<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');

require 'service/subjectservice.php';
$subjectservice = new Subjectservice($connection);
$topic_id = $_GET['topic_id'];
$topic_name = $_GET['topic_name'];
?>

<!-- <script>

  function enableDisableAll(e,subject_id) {

        var own = e;
        var form = document.getElementById("subjecteditform");
        var elements = form.elements;
        var elementslength = elements.length;
        
          if(own.checked == true){
              form.hidden = false;
                    
                  
          }else{
              form.hidden = true;
                     
          }

}

</script> -->


<div class="modal fade" id="addsubject" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Subject Data</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="subject_code.php?topic_id=<?php echo $topic_id; ?>&topic_name=<?php echo $topic_name; ?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">

          <div class="form-group">
            <label> Subject: </label>
            <input type="text" name="subject" class="form-control subject" placeholder="Enter Subject Name..">
          </div>
          <div class="form-group">
            <label>Upload Image:</label>
            <input type="file" name="subject_image" class="form-control" required>
          </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button type="submit" name="addbtn" class="btn btn-primary">Add</button>
        </div>
      </form>

    </div>
  </div>
</div>


<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">

    <div class="card-header py-3 mb-4">
      <h6 class="m-0 font-weight-bold text-primary text-uppercase"><?php echo $topic_name; ?> </h6>
    </div>


    <!-- Content Row -->
    <div class="container-fluid">

      <div class="d-sm-flex align-items-center justify-content-between mb-8">
        <h1 class="h3 mb-0 text-gray-800">SUBJECTS</h1>
        <button type="button" data-toggle="modal" data-target="#addsubject" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus-circle fa-sm text-white-50"></i> Add Subjects</button>
      </div>
    </div>


    <div class="card-body">
      <?php
      if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
        echo '<h2>' . $_SESSION['success'] . '</h2>';
        unset($_SESSION['success']);
      }
      if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
        echo '<h2>' . $_SESSION['status'] . '</h2>';
        unset($_SESSION['status']);
      }
      ?>


      <?php
      $results = $subjectservice->getSubjects($topic_id);
      foreach ($results as $subject) {
        $subject_id = $subject["id"];
        $photourl = $subject["photourl"];
      ?>

        <div class="card border-left-warning shadow h-100 py-2 mb-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center ">

              <img id="sub_img_tag" src="<?php echo $photourl; ?>" alt="subject Image" height="100px" width="100px">

              <div class="col mr-2 ml-2">
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php echo ($subject['name']); ?>
                </div>

                <div class="h5 mb-0 font-weight-normal text-gray-800">
                  <a href="chapters.php?topic_id=<?php echo $topic_id ?>&subject_id=<?php echo $subject_id ?> &&subject_name=<?php echo $subject['name'] ?>">
                    <?php echo "no of chapters: " . ($subject['no_of_chapters']); ?>
                  </a>
                </div>
              </div>

              <form action="subject_edit.php?topic_id=<?php echo $topic_id; ?>&topic_name=<?php echo $topic_name; ?>" method="post">
                <input type="hidden" name="subject_id" value="<?php echo $subject['id']; ?>">
                <input type="hidden" name="subject_name" value="<?php echo $subject['name']; ?>">
                <input type="hidden" name="photourl" value="<?php echo $subject['photourl']; ?>">
                <button type="submit" name="editsubjectbtn" class="h5-inline-block btn btn-info">Edit</button>
              </form>

              <div>

              </div>

            </div>
          </div>
        </div>
      <?php
      }

      ?>

    </div>
  </div>
</div>

<?php
include('includes/scripts.php');
?>