<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
$topic_id = $_GET['topic_id'];
$subject_id = $_GET['subject_id'];
?>

</script>

<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> Question Details </h6>
        </div>
        <div class="card-body">
            <?php

            if (isset($_POST['editchapterbtn'])) {
                $chapter_id = $_POST['chapter_id'];
                $chapter_name = $_POST['chapter_name'];
                $live_date = $_POST['live_date'];

                $timer = $_POST['timer'];
                $hours = floor($timer / 3600);
                $minutes = floor(($timer / 60) % 60);
            ?>

                <form id="chaptereditform" action="chapter_code.php?topic_id=<?php echo $topic_id; ?>&subject_id=<?php echo $subject_id; ?>" method="POST">
                                  <div class="form-body">
                                    <div class="form-group">
                                      <input type="text" name="chapter_id" id="chapter_id" class="form-control" value="<?php echo $chapter_id ?>" hidden>
                                    </div>

                                    <div class="form-group">
                                      <label> Chapter: </label>
                                      <input type="text" name="chapter_name" class="form-control" value="<?php echo $chapter_name;?>">
                                    </div>
                                    <div class="form-group">
                                      <label> Edit Live date: </label>
                                      <input type="date" name="livedate" class="form-control" value="<?php echo $live_date;?>">
                                    </div>
                                    <div class="form-group">
                                      <label> Edit Time of test: </label>
                                      <input type="number" name="testtimehr" class="form-control mb-2" value="<?php echo $hours;?>" min="0" max="23">
                                      <input type="number" name="testtimemin" class="form-control" value="<?php echo $minutes;?>" min="0" max="59">
                                    </div>

                                  <div class="form-footer">
                                    <button type="submit" name="chaptereditbtn" class="btn btn-primary" value="edit">Edit</button>
                                  </div>
                                </div>
              </form>
            <?php
            }
            ?>
        </div>
    </div>
</div>

</div>

<?php

include('includes/scripts.php');
?>