<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
// require 'service/subjectservice.php';
require 'service/chapterservice.php';
require 'service/questionservice.php';
$chapterservice = new ChapterService($connection);
// $subjectservice = new SubjectService($connection);
$questionservice = new QuestionService($connection);
// $topic_id = $_GET['topic_id'];
// $subject_id = $_GET['subject_id'];
$chapter_id = $_GET['chapter_id'];
$chaptername = ($chapter_id == 0) ? 'MCQ Of The Day' : $chapterservice->getChapterName($chapter_id);
?>
<script type="text/javascript">
function triggerClick1(){
  document.querySelector('#question_image1').click();
}
function triggerClick2(){
  document.querySelector('#question_image2').click();
}
function triggerClick3(){
  document.querySelector('#question_image3').click();
}

function displayImage1(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay1').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function displayImage2(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay2').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function displayImage3(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay3').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
</script>




<div class="modal fade" id="addquestion" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add Questions</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form action="question_code.php?chapter_id=<?php echo $chapter_id; ?>" method="POST" enctype="multipart/form-data">
        <div class="modal-body">
          <div class="form-group">
            <label style="color:black"><b>Question:</b></label>
            <input type="text" name="enter_question" class="form-control" placeholder="Enter question here...">
          </div>

          <div class="form-group">
            <label style="color:black"><b>Upload images for the question:</b></label><br>
            <img src="SynapseBackend/Images/question/placeholder.png" onclick="triggerClick1()" id="imageUploadDisplay1" name="imageUploadDisplay1" height="100px" width="100px" >
            <input type="file" name="question_image1" id="question_image1" class="form-control" onchange="displayImage1(this)" style="display:none">

            <img src="SynapseBackend/Images/question/placeholder.png" onclick="triggerClick2()" id="imageUploadDisplay2" name="imageUploadDisplay2" height="100px" width="100px" >
            <input type="file" name="question_image2" id="question_image2" class="form-control" onchange="displayImage2(this)" style="display:none">

            <img src="SynapseBackend/Images/question/placeholder.png" onclick="triggerClick3()" id="imageUploadDisplay3" name="imageUploadDisplay3" height="100px" width="100px" >
            <input type="file" name="question_image3" id="question_image3" class="form-control" onchange="displayImage3(this)" style="display:none">
          </div>

          <div class="form-group">
            <label style="color:black"><b>Option 1:</b></label>
            <input type="text" name="option_1" class="form-control" placeholder="Enter option 1 here...">
          </div>
          <div class="form-group">
            <label style="color:black"><b>Option 2:</b></label>
            <input type="text" name="option_2" class="form-control" placeholder="Enter option 2 here...">
          </div>
          <div class="form-group">
            <label style="color:black"><b>Option 3:</b></label>
            <input type="text" name="option_3" class="form-control" placeholder="Enter option 3 here...">
          </div>
          <div class="form-group">
            <label style="color:black"><b>Option 4:</b></label>
            <input type="text" name="option_4" class="form-control" placeholder="Enter option 4 here...">
          </div>
          <div class="form-group">
            <label style="color:black"><b>Answer:</b></label>
            <select name="answer" placeholder="Choose correct answer">
              <option value="" disabled selected> Select correct answer... </option>
              <option value="1">Option 1</option>
              <option value="2">Option 2</option>
              <option value="3">Option 3</option>
              <option value="4">Option 4</option>
            </select>
          </div>
          <div class="form-group">
            <label style="color:black"><b>Explanation:</b></label>
            <div id="explanation_editor" style="max-height:500px;overflow:auto"></div>
            <input type="hidden" name="quill_html" id="quill_html">
            <!-- <input type="text" name="explanation" class="form-control" placeholder="Enter explanation here..."> -->
          </div>

          <?php if ($chapter_id == 0) {
          ?>
            <div class="form-group">
              <label> Enter Live date: </label>
              <input type="date" name="livedate" class="form-control">
            </div>
          <?php
          }
          ?>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          <button id="questionaddbtn" type="submit" name="questionaddbtn" class="btn btn-primary">Add</button>
        </div>
      </form>
    </div>
  </div>
</div>



<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">

    <div class="card-header py-3 mb-4">
      <h6 class="m-0 font-weight-bold text-primary text-uppercase"><?php echo $chaptername; ?></h6>
    </div>


    <!-- Content Row -->
    <div class="container-fluid">

      <?php if ($chapter_id == 0) {
      ?> <div class="d-sm-flex align-items-center justify-content-between mb-8">
          <h1 class="h3 mb-2 text-gray-800">Today's MCQ</h1>
          <button type="button" data-toggle="modal" data-target="#addquestion" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus-circle fa-sm text-white-50"></i> Add questions </button>
        </div>
      <?php
      }
      ?>
      <?php
      if ($chapter_id == 0) {
        $results = $questionservice->getTodayMCQ();
        if (sizeof($results) > 0) {
          foreach ($results as $question) {
            $question_id = $question['id'];
      ?>
            <h1 class="h3 mb-3 text-gray-500"></h1>
            <div class="card border-left-warning shadow h-100 py-2 mb-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center ">
                  <div class="col mr-2">
                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                      <?php echo ($question['question']); ?>
                    </div>
                    <div class="h5 mb-0 font-weight-normal text-red-500">
                      <?php $currentDate = new DateTime($question['created_date']);
                      echo ('Scheduled Date: ' . $currentDate->format('D M d, Y')); ?>
                    </div>
                    <!-- must add edit btn -->
                  </div>
                </div>
              </div>
            </div>

            <h1 class="h3 mb-5 text-gray-500"></h1>
          <?php
          }
        } else {
          ?>
          <div class="d-sm-flex align-items-center justify-content-center mb-8">
            <h1 class="h3 m-3 mb-5 mt-5 text-gray-500">No Question Scheduled for today</h1>
          </div>
      <?php
        }
      }

      ?>
      <div class="d-sm-flex align-items-center justify-content-between mb-8">
        <h1 class="h3 mb-0 text-gray-800">Questions</h1>
        <?php if ($chapter_id != 0) {
        ?> <button type="button" data-toggle="modal" data-target="#addquestion" class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm"><i class="fas fa-plus-circle fa-sm text-white-50"></i> Add questions </button>
        <?php
        }
        ?>
      </div>


    </div>


    <div class="card-body">
      <?php
      if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
        echo '<h2>' . $_SESSION['success'] . '</h2>';
        unset($_SESSION['success']);
      }
      if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
        echo '<h2>' . $_SESSION['status'] . '</h2>';
        unset($_SESSION['status']);
      }
      ?>

      <?php
      $results = $questionservice->getQuestionByChapterId($chapter_id);
      foreach ($results as $question) {
        $question_id = $question['id'];
        $date = new DateTime();
        $currentDate = $date->format('Y-m-d');
        if ($chapter_id == 0) {
          if ($question['created_date'] != $currentDate) {
      ?>
            <div class="card border-left-warning shadow h-100 py-2 mb-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center ">
                  <div class="col mr-2">
                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                      <?php echo ($question['question']); ?>
                    </div>
                    <div class="h5 mb-0 font-weight-normal text-red-500">
                      <?php $currentDate = new DateTime($question['created_date']);
                      echo ('Scheduled Date: ' . $currentDate->format('D M d, Y')); ?>
                    </div>
                  </div>

                  <!-- must add edit btn -->
                  <div class="col-auto">
                    <form action="question_details.php?chapter_id=<?php echo $chapter_id; ?>" method="post">
                      <input type="hidden" name="isReported" value="<?php echo false; ?>">
                      <input type="hidden" name="question_id" value="<?php echo $question['id']; ?>">
                      <button type="submit" name="details_btn" class="h5-inline-block btn btn-success"> View details</button>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          <?php
          }
        } else {
          ?>
          <div class="card border-left-warning shadow h-100 py-2 mb-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center ">

                <div class="col mr-2">
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php echo ($question['question']); ?>
                  </div>
                </div>

                <!-- must add edit btn -->
                <div class="col-auto">
                  <form action="question_details.php?chapter_id=<?php echo $chapter_id; ?>" method="post">
                    <input type="hidden" name="isReported" value="<?php echo false; ?>">
                    <input type="hidden" name="question_id" value="<?php echo $question['id']; ?>">
                    <button type="submit" name="details_btn" class="btn btn-success"> View details</button>
                  </form>
                </div>

              </div>
            </div>
          </div>
      <?php

        }
      }

      ?>

    </div>
  </div>
</div>


</div>


<?php
include('includes/scripts.php');
?>