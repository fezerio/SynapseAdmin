<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
include('service/userservice.php');

?>

<div class="modal fade" id="addadminprofile" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Add Admin Data</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="register_admin_code.php" method="POST">
                <div class="modal-body">

                    <div class="form-group">
                        <label> Username </label>
                        <input type="text" name="username" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" name="email" class="form-control checking_email" placeholder="Enter Email">
                        <small class="error_email" style="color: red;"></small>
                    </div>
                    <div class="form-group">
                        <label>Password</label>
                        <input type="password" name="password" class="form-control" placeholder="Enter Password">
                    </div>
                    <div class="form-group">
                        <label>Confirm Password</label>
                        <input type="password" name="confirmpassword" class="form-control" placeholder="Confirm Password">
                    </div>


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" name="registerbtn" class="btn btn-primary">Save</button>
                </div>
            </form>

        </div>
    </div>
</div>


<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
              <div class="d-sm-flex align-items-center justify-content-between mb-8">
                <h4 class="m-0 font-weight-bold text-primary">Admin Profile</h6>
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addadminprofile">Add Admin Profile
                </button>
              </div>
        </div>

        <div class="card-body">

            <?php
            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo '<h2>' . $_SESSION['success'] . '</h2>';
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo '<h2>' . $_SESSION['status'] . '</h2>';
                unset($_SESSION['status']);
            }
            ?>

            <div class="table-responsive">
                <?php
                $userservice = new UserService($connection);

                $query_run = $userservice->getAdminData();
                ?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th> ID </th>
                            <!-- <th> Name </th> -->
                            <th> Username </th>
                            <th> Email</th>
                            <!--  <th>UserType</th> -->
                            <th>Created Date</th>
                            <th>Edit</th>
                            <th>DELETE</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (mysqli_num_rows($query_run) > 0) {
                            while ($row = mysqli_fetch_assoc($query_run)) {
                        ?>
                                <tr>
                                    <td><?php echo  $row['id'] ?></td>
                                    <!-- <td><?php echo $row['name']; ?></td> -->
                                    <td><?php echo $row['firebase_uid']; ?></td>
                                    <td><?php echo $row['email_id']; ?></td>
                                    <td><?php $date = new DateTime($row['created_date']);
                                        echo $date->format('D M d, Y');
                                        ?></td>
                                    <!--  <td><?php echo $row['usertype']; ?></td> -->
                                    <td>
                                        <form action="register_admin_edit.php" method="post">
                                            <input type="hidden" name="firebase_uid" value="<?php echo $row['firebase_uid']; ?>">
                                            <input type="hidden" name="email_id" value="<?php echo $row['email_id']; ?>">
                                            <button type="submit" name="edit_btn" class="btn btn-success"> EDIT</button>
                                        </form>
                                    </td>
                                    <td>
                                        <form action="register_admin_code.php" method="post">
                                            <input type="hidden" name="delete_id" value="<?php echo $row['firebase_uid']; ?>">
                                            <button type="submit" name="delete_btn" class="btn btn-danger"> DELETE</button>
                                        </form>
                                    </td>
                                </tr>
                        <?php
                            }
                        } else {
                            echo "No Record Found";
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php

include('includes/scripts.php');
?>