<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
$topic_id = $_GET['topic_id'];
$topic_name = $_GET['topic_name'];
?>

<!-- <script type="text/javascript">
    function changebck(this){
        var value = this
        var image = document.getElementById('sub_img_tag');
        image.src = ""
    }

</script> -->

<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> EDIT Admin Profile </h6>
        </div>
        <div class="card-body">
            <?php

                if (isset($_POST['editsubjectbtn'])) {
                    $subject_id = $_POST['subject_id'];
                    $subject_name = $_POST['subject_name'];
                    $photourl = $_POST['photourl'];

            ?>

                <form action="subject_code.php?topic_id=<?php echo $topic_id; ?>&topic_name=<?php echo $topic_name; ?>" method="POST" enctype="multipart/form-data">

                    <input type="hidden" name="subject_id" value="<?php echo $subject_id ?>">

                    <div class="form-group">
                        <label> Subject name: </label>
                        <input type="text" name="subject_name" value="<?php echo $subject_name ?>" class="form-control">
                    </div>
                    <!-- no file selected case left to do  -->
                    <div class="form-group">
                        <label>Upload Image:</label> 
                        <input type="file" name="subject_image" id="subject_image" class="form-control" value="<?php echo $photourl;?>" >
                    </div>

                    <button type="submit" name="updatebtn" id="updatebtn" class="btn btn-primary"> Update </button>

                </form>
            <?php
                // }
            }
            ?>
        </div>
    </div>
</div>

</div>

<?php

include('includes/scripts.php');
?>