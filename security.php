<?php
session_start();
include('service/dbconnect.php');
if ($connection) {
    // echo "Database Connected";
} else {
    header("Location: service/dbconnect.php");
}

if (!$_SESSION['username']) {
    header('Location: login.php');
}
