<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
require 'service/subjectservice.php';
$subjectservice = new Subjectservice($connection);
?>

<style type="text/css">
  .dropdown-InputBox {
    background-color: #FFF;
    border-radius: 4px;
    border: #bdbdbd 1px solid;
    padding: 10px;
    width: 50%;
</style>

<script type="text/javascript">
  /* <!-- $(document).ready(function(){ --> */
  //     function loadData(){
  //         $.ajax({
  //             url: "service/getchapters.php",
  //             method: "post",
  //             data: {subject_id: subject},
  //             success : function(data){
  //                 $("#country").append(data);
  //             }
  //         })
  //     }
  // })
  $(document).ready(function() {
    $("#subject").change(function() {
      var subject_id = $("#subject").val();
      $.ajax({
        url: 'service/chapterservice.php',
        method: 'post',
        data: {
          'subject_id': subject_id,
          'topic_id': 1
        },
        //1 for basic, 2 for major 3 for minor

      }).done(function(chapter) {
        // console.log(document.getElementById("chapter").option);
        console.log(chapter);
        chapter = JSON.parse(chapter);
        $('#chapter').empty();
        chapter.forEach(function(chapter) {
          $('#chapter').append('<option>' + chapter.name + '</option>')
        })
      })
    })
  })
</script>

<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"> BASIC PRACTICE SETS </h6>
    </div>

    <!-- Content Row -->
    <div class="row">

      <!-- Total Registered User Card -->
      <div class="col-xl-2 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center">
              <div class="col mr-2">
                <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Add Chapters and Subject</div>
              </div>
              <div class="col-auto">
                <i class="fas fa-plus-circle fa-2x text-gray-300"></i>
              </div>
            </div>
          </div>
        </div>
      </div>

      <!-- Total Registered Admin Card  -->
      <!--  <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Total Registered Admin</div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    11
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-users-cog fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div> -->

      <!-- Total Questions Card -->
      <!--  <div class="col-xl-3 col-md-6 mb-4">
          <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
              <div class="row no-gutters align-items-center">
                <div class="col mr-2">
                  <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Total Questions </div>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    11
                  </div>
                </div>
                <div class="col-auto">
                  <i class="fas fa-book fa-2x text-gray-300"></i>
                </div>
              </div>
            </div>
          </div>
        </div> -->

      <!-- Post Slider -->
      <!--         <div class="post-slider">
            <div class="Post-wrapper">
                <div class="post">1</div>

            </div>
        </div> -->
      <!-- End of post slider -->
    </div>


    <div class="card-body">

      <form action="basic_code.php" method="POST" onkeydown="return event.key != 'Enter';">
        <div class="form-group">
          <label style="color: black"><b>Subject:</b></label>
          <select aut name="subject" id="subject" class="dropdown-InputBox">
            <option value disabled selected> Select Subject .. </option>
            <?php
            $results = $subjectservice->getSubjects(1);
            foreach ($results as $subject) {
              echo "<option id='" . $subject['id'] . "' value='" . $subject['id'] . "'>" . $subject['name'] . "</option>";
            }
            ?>
          </select>
        </div>

        <html>

        <body>
          <?php
          $results = $subjectservice->getSubjects(1);
          foreach ($results as $subject) {
          ?>
            <div class="card border-left-warning shadow h-100 py-2">
              <div class="card-body">
                <div class="row no-gutters align-items-center">
                  <div class="col mr-2">
                    <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Subjet Name </div>
                    <div class="h5 mb-0 font-weight-bold text-gray-800">
                      <?php echo ($subject['name']); ?>
                    </div>
                  </div>
                  <div class="col-auto">
                    <i class="fas fa-book fa-2x text-gray-300"></i>
                  </div>
                </div>
              </div>
            </div>
          <?php
          }


          ?>
        </body>

        </html>
        <div class="form-group">
          <label style="color:black"><b>Chapter:</b></label>
          <select name="chapter" id="chapter" class="dropdown-InputBox">
            <option value="" disabled selected> Select Chapter .. </option>
          </select>
        </div>

        <div class="form-group">
          <label style="color:black"><b>Question:</b></label>
          <input type="text" name="enter_question" class="form-control" placeholder="Enter question here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 1:</b></label>
          <input type="text" name="option_1" class="form-control" placeholder="Enter option 1 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 2:</b></label>
          <input type="text" name="option_2" class="form-control" placeholder="Enter option 2 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 3:</b></label>
          <input type="text" name="option_3" class="form-control" placeholder="Enter option 3 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Option 4:</b></label>
          <input type="text" name="option_4" class="form-control" placeholder="Enter option 4 here...">
        </div>
        <div class="form-group">
          <label style="color:black"><b>Answer:</b></label>
          <select name="answer" placeholder="Choose correct answer">
            <option value="" disabled selected> Select correct answer... </option>
            <option value="1">Option 1</option>
            <option value="2">Option 2</option>
            <option value="3">Option 3</option>
            <option value="4">Option 4</option>
          </select>
          <!-- <label  style="color:black"><b>Answer:</b></label>
                                <input list="options" name="answer" class="form-control" readonly placeholder="Choose the correct answer">
                                <datalist id="options">
                                  <option value="1">
                                  <option value="2">
                                  <option value="3">
                                  <option value="4">
                                </datalist> -->
        </div>
        <div class="form-group">
          <label style="color:black"><b>Explaination:</b></label>
          <input contenteditable="true" type="text" name="explaination" class="form-control" placeholder="Enter explaination here...">
        </div>

        <a href="register_admin.php" class="btn btn-danger"> CANCEL </a>
        <button type="submit" name="submitbtn" class="btn btn-primary"> Submit </button>

      </form>

    </div>
  </div>
</div>

</div>


<?php
include('includes/scripts.php');
?>