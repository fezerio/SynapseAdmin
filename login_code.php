<?php
include('security.php');
include('service/authentication.php');

if (isset($_POST['login_btn'])) {
     $email_login =  $_POST['email'];
     $password_login = $_POST['password'];
     // echo json_encode('dsdsa');
     // $sql1 = "SELECT name,email_id,created_date from user where firebase_uid = '{$email_login}' && admin_password = '{$password_login}'";
     // $r = mysqli_query($connection, $sql1);
     // $row = mysqli_fetch_assoc($r);
     $adminAuthentication = new Authentication($email_login, $password_login, $connection);
     $result = $adminAuthentication->loginAdmin();
     //     $query = "SELECT * FROM admin WHERE email='$email_login' AND password='$password_login' LIMIT 1";
     //     $query_run = mysqli_query($connection, $query);

     if ($result) {
          $_SESSION['username'] = $result['firebase_uid'];
          $_SESSION['emailId'] = $result['email_id'];
          $_SESSION['user_id'] = $result['firebase_uid'];
          header('Location: index.php');
     } else {
          $_SESSION['status'] = "Username / Password is Invalid";
          header('Location: login.php');
     }
}
