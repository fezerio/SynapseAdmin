<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
// require 'service/subjectservice.php';
// require 'service/chapterservice.php';
require 'service/questionservice.php';
// $chapterservice = new ChapterService($connection);
// $subjectservice = new SubjectService($connection);
$questionservice = new QuestionService($connection);
// // $topic_id = $_GET['topic_id'];
// // $subject_id = $_GET['subject_id'];
// $chapter_id = $_GET['chapter_id'];
// $chaptername = ($chapter_id == 0) ? 'MCQ Of The Day' : $chapterservice->getChapterName($chapter_id);
?>


<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">

    <div class="card-header py-3 mb-4">
      <h6 class="m-0 font-weight-bold text-primary text-uppercase"><?php echo ('Reported Question') ?></h6>
    </div>


    <div class="card-body">
      <?php
      if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
        echo '<h2>' . $_SESSION['success'] . '</h2>';
        unset($_SESSION['success']);
      }
      if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
        echo '<h2>' . $_SESSION['status'] . '</h2>';
        unset($_SESSION['status']);
      }
      ?>

      <?php
      $results = $questionservice->getReportedQuestions();
      foreach ($results as $question) {
        $question_id = $question['id'];
        $chapter_id = $question['chapter_id'];
      ?>
        <div class="card border-left-warning shadow h-100 py-2 mb-2">
          <div class="card-body">
            <div class="row no-gutters align-items-center ">
              <div class="col mr-2">
                <a class="h5 mb-0 font-weight-bold text-blue-800" href="questions.php?chapter_id=<?php echo $chapter_id ?>">
                  <div class="h5 mb-0 font-weight-bold text-blue-800">
                    <?php echo ($chapter_id == 0 ? strtoupper('MCQ Of the Day') : strtoupper($question['chapter_name'])); ?>
                  </div>
                </a>
                <div class="h5 mb-0 font-weight-bold text-gray-800">
                  <?php echo ($question['question']); ?>
                  <div class="h5 mb-0 font-weight-bold text-gray-800">
                    <?php echo 'No of Times: ' . ($question['count']); ?>
                  </div>
                </div>
              </div>

              <!-- must add edit btn -->
              <div class="col-auto">
                <form action="question_details.php?chapter_id=<?php echo $question['chapter_id']; ?>" method="post">
                  <input type="hidden" name="question_id" value="<?php echo $question['id']; ?>">
                  <input type="hidden" name="isReported" value="<?php echo true; ?>">
                  <button type="submit" name="details_btn" class="btn btn-success"> View details</button>
                </form>
              </div>

            </div>
          </div>
        </div>
      <?php
      }
      ?>
    </div>
  </div>
</div>


</div>


<?php
include('includes/scripts.php');
?>