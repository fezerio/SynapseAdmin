<?php
include_once 'dbconnect.php';
//0,0 for daily test
//-1,-1 for mock test
$topicId = $_POST['topic_id'];
$subjectId = $_POST['subject_id'];
$idValue = '';

if (($topicId == -1 && $subjectId == -1) || ($topicId == 0 && $subjectId == 0))
      $subjectSQL = "SELECT id from chapter where topic_id='{$topicId}' AND subject_id='{$subjectId}'";
else
      $subjectSQL = "SELECT id from chapter where topic_id NOT IN (0,-1) AND subject_id NOT IN (0,-1)";


$r1 = mysqli_query($dbsel, $subjectSQL);
while ($row = mysqli_fetch_assoc($r1))
      $idValue = $idValue . ',' . $row['id'];
$temp = ltrim($idValue, ',');

$sql = "SELECT user.name AS name,user.photo_url AS photo_url, user_id, (SELECT COUNT(*) from question q where q.chapter_id IN ($temp)) AS total,SUM(CASE WHEN qt.correct_answer = qt.user_answer THEN 1 ELSE 0 END) AS right_answer,
      SUM(CASE WHEN qt.correct_answer != qt.user_answer THEN 1 ELSE 0 END) AS wrong_answer from question_tracking qt inner JOIN user ON qt.user_id=user.firebase_uid WHERE qt.chapter_id IN ($temp) GROUP BY qt.user_id order by right_answer desc";
$r = mysqli_query($dbsel, $sql);
$data = array();
while ($row = mysqli_fetch_assoc($r))
      $data[] = $row;
echo json_encode($data);
