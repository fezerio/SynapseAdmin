<?php
include_once 'dbconnect.php';
$userId = 
// 'Ogx3HEEM0eMxESRBKThJXrjoAYj2';
$_POST['user_id'];
$date = new DateTime();
$finalDate = new DateTime();
$finalDate->add(new DateInterval('P4D'));
$currentDate = json_encode($date->format('Y-m-d'));
$maxDate = json_encode($finalDate->format('Y-m-d'));
$sql = "SELECT c.id,c.name,c.topic_id,c.subject_id,c.created_date,c.live_date, 
(Select COUNT(*) FROM question q where q.chapter_id =c.id) AS no_of_questions, 
c.timer,(CASE WHEN (Select COUNT(*) FROM question q where q.chapter_id =c.id)<=
(Select COUNT(*) FROM question_tracking qt where qt.chapter_id =c.id && qt.user_id ='{$userId}') 
THEN 1 ELSE 0 END)AS completed, (CASE WHEN (Select COUNT(*) FROM question_tracking qt where 
qt.chapter_id =c.id && qt.user_id ='{$userId}')>0 && (Select COUNT(*) FROM question q where 
q.chapter_id =c.id)>(Select COUNT(*) FROM question_tracking qt where qt.chapter_id =c.id && 
qt.user_id ='{$userId}') THEN 1 ELSE 0 END)AS not_completed from chapter c WHERE c.topic_id=-1 
&& c.subject_id=-1 && (DATEDIFF(live_date,$currentDate) = 0) UNION (SELECT c2.id,c2.name,c2.topic_id,
c2.subject_id,c2.created_date,c2.live_date, (Select COUNT(*) FROM question q where q.chapter_id =c2.id) AS no_of_questions,
c2.timer,(CASE WHEN (Select COUNT(*) FROM question q where q.chapter_id =c2.id)<=(Select COUNT(*) FROM question_tracking qt
where qt.chapter_id =c2.id && qt.user_id ='{$userId}') THEN 1 ELSE 0 END)AS completed, (CASE WHEN (Select COUNT(*) FROM 
question_tracking qt where qt.chapter_id =c2.id && qt.user_id ='{$userId}')>0 && (Select COUNT(*) FROM question q where
q.chapter_id =c2.id)>(Select COUNT(*) FROM question_tracking qt where qt.chapter_id =c2.id && qt.user_id ='{$userId}') 
THEN 1 ELSE 0 END)AS not_completed FROM chapter c2 WHERE c2.topic_id=-1 && c2.subject_id=-1 && (DATEDIFF(live_date,$currentDate) >-1 &&
DATEDIFF(live_date,$currentDate) < 5) ORDER BY live_date asc LIMIT 4)";
$r = mysqli_query($dbsel, $sql);
$data = array();

while ($row = mysqli_fetch_assoc($r)) {
	$data[] = $row;
}

echo json_encode($data);
