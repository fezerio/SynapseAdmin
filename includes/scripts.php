 <!-- Bootstrap core JavaScript-->
 <script src="vendor/jquery/jquery.min.js"></script>
 <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

 <!-- Core plugin JavaScript-->
 <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

 <!-- Custom scripts for all pages-->
 <script src="js/sb-admin-2.min.js"></script>

 <!-- Page level plugins -->
 <script src="vendor/chart.js/Chart.min.js"></script>

 <!-- Page level custom scripts -->
 <script src="js/chart.js"></script>
 <script src="js/pie-chart.js"></script>

 <!-- Font awesome -->
 <script src="https://kit.fontawesome.com/ef8e182c6b.js" crossorigin="anonymous"></script>


 <script type="text/javascript">
   var today = new Date().toISOString().split('T')[0];
   document.getElementsByName("livedate")[0].setAttribute('min', today);
 </script>

  <script src="//cdn.quilljs.com/1.2.2/quill.min.js"></script>
  <script src="quill_editor/image-resize.min.js"></script>

<script>
 var toolbarOptions = [
                ['bold', 'italic', 'underline', 'strike'],        // toggled buttons
                ['blockquote', 'code-block'],

                [{ 'header': 1 }, { 'header': 2 }],               // custom button values
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [{ 'script': 'sub'}, { 'script': 'super' }],      // superscript/subscript
                [{ 'indent': '-1'}, { 'indent': '+1' }],          // outdent/indent
                [{ 'direction': 'rtl' }],                         // text direction

                [{ 'size': ['small', false, 'large', 'huge'] }],  // custom dropdown
                [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
                [ 'link', 'image', 'formula' ],          // add's image support
                [{ 'color': [] }, { 'background': [] }],          // dropdown with defaults from theme
                [{ 'font': [] }],
                [{ 'align': [] }],

                ['clean']                                         // remove formatting button
            ];

  var quill = new Quill("#explanation_editor", {
      modules: {
            imageResize: {
              displaySize: true
            },
          toolbar: toolbarOptions,                   
      },
      theme: 'snow',       
  });

  $('#questionaddbtn').on('click', () => { 
    // Get HTML content
    var html = quill.root.innerHTML;

    // Copy HTML content in hidden form
    $('#quill_html').val( html )

    // Post form
    myForm.submit();
})

                 
</script>





<script>

  var quill1 = new Quill("#explanation_editor_edit", {
      modules: {
            imageResize: {
              displaySize: true
            },
          toolbar: toolbarOptions,                   
      },
      theme: 'snow',       
  });

  $(document).ready(function(){
    quill1.enable(false);
    quill1.root.innerHTML = $("#quill1_html").val();
  });


  $('#questioneditbtn').on('click', () => { 
    // Get HTML content
    var html = quill1.root.innerHTML;

    // Copy HTML content in hidden form
    $('#quill1_html').val( html )

    // Post form
    myForm.submit();
  })
                  
</script>

 <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
 <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script> -->
 <!-- <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.3/js/bootstrap.min.js" integrity="sha384-w1Q4orYjBQndcko6MimVbzY0tgp4pWB4lZ7lr30WKz0vr/aWKhXdBNmNb5D92v7s" crossorigin="anonymous"></script> -->