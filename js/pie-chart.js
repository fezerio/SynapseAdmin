// Set new default font family and font color to mimic Bootstrap's default styling
Chart.defaults.global.defaultFontFamily = 'Nunito', '-apple-system,system-ui,BlinkMacSystemFont,"Segoe UI",Roboto,"Helvetica Neue",Arial,sans-serif';
Chart.defaults.global.defaultFontColor = '#858796';

let xhr1 = new XMLHttpRequest();
$url = '/web/SynapseAdmin/service/getUserTrackingSummary.php';
// '/service/getUserTrackingSummary.php';
xhr1.open('get', $url, true);
xhr1.send();
xhr1.onload = function() {
    var response = JSON.parse(xhr1.response);
    console.log(response);
    var data = [response['attempted_question'], response['left_question'], response['correct_answer'], response['incorrect_answer']];
    var ctx = document.getElementById("myPieChart");
    var myPieChart = new Chart(ctx, {
        type: 'doughnut',
        data: {
            labels: ["Attempted Question", "Skipped Question", "Correct Answer", "Incorrect Answer"],
            datasets: [{
                data: data,
                backgroundColor: ['#4e73df', '#36b9cc', '#1cc88a', '#ff0000'],
                hoverBackgroundColor: ['#2e59d9', '#2c9faf', '#17a673', '#801818'],
                hoverBorderColor: "rgba(234, 236, 244, 1)",
            }],
        },
        options: {
            maintainAspectRatio: false,
            tooltips: {
                backgroundColor: "rgb(255,255,255)",
                bodyFontColor: "#858796",
                borderColor: '#dddfeb',
                borderWidth: 1,
                xPadding: 15,
                yPadding: 15,
                displayColors: false,
                caretPadding: 10,
            },
            legend: {
                display: false
            },
            cutoutPercentage: 80,
        },
    });
};