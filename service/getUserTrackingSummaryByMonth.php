<?php
include('../security.php');

$sql = "SELECT MONTH(qt.created_date) AS month, COUNT(*) AS count from question_tracking qt GROUP BY MONTH(qt.created_date)";
$r = mysqli_query($connection, $sql);
$data = array();
while ($row = mysqli_fetch_assoc($r))
    $data[] = $row;
echo json_encode($data);
