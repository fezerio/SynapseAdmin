<?php
require 'imageservice.php';

class QuestionService
{
    public $dbsel;


    function __construct($db)
    {
        $this->dbsel = $db;
    }

    function getTotalNumberOfQuestions()
    {
        $sql = "SELECT COUNT(*) AS count from question";
        $r = mysqli_query($this->dbsel, $sql);
        $row = mysqli_fetch_assoc($r);
        return $row['count'];
    }


    function insertQuestion($chapter_id, $question, $option_a, $option_b, $option_c, $option_d, $answer, $explanation)
    {
        $trimmed_explanation = str_replace("'", '', $explanation);

        $sql = "INSERT INTO question (chapter_id,question,option_a,option_b,option_c,option_d,answer,explanation)VALUES ('{$chapter_id}','{$question}','{$option_a}','{$option_b}','{$option_c}','{$option_d}','{$answer}','{$trimmed_explanation}')";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r) {
            $sql2 = "SELECT LAST_INSERT_ID() AS id";
            $r2 = mysqli_query($this->dbsel, $sql2);
            if ($r2) {
                $row = mysqli_fetch_assoc($r2);
                $questionId = $row['id'];
                return $questionId;
            } else {
                return '';
            }
        } else return '';
    }




    function getReportedQuestions()
    {
        $sql = "SELECT q.id, q.chapter_id, (SELECT name from chapter c where c.id=q.chapter_id) AS chapter_name, q.question, (SELECT COUNT(*) FROM reported_question rp where rp.question_id=q.id) AS count FROM question q where q.id IN(SELECT DISTINCT(r.question_id) FROM reported_question r)";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r))
            $data[] = $row;
        return $data;
    }

    function getQuestionByChapterId($chapterId)
    {
        $mcqQuery = (($chapterId == 0) ? 'created_date desc' : 'id asc');
        $sql = "SELECT * from question where chapter_id ='{$chapterId}' ORDER BY  {$mcqQuery}";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r)) {
            $data[] = $row;
        }
        return $data;
    }

    function getQuestionByQuestionId($questionId)
    {
        $sql = "SELECT * from question where id ='{$questionId}'";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r)) {
            // echo json_encode($row);
            // $q = new QuestionModel($row['id'], $row['chapter_id'], $row['question'], $row['option_a'], $row['option_b'], $row['option_c'], $row['option_d'], $row['answer'], $row['explanation'], $row['created_date']);
            $data[] = $row;
        }
        return $data;
    }

    function getTodayMCQ()
    {
        $date = new DateTime("now", new DateTimeZone("UTC"));
        $currentDate = $date->format('Y-m-d');
        $sql = "SELECT * from question where chapter_id =0 && created_date ='{$currentDate}'";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r)) {
            $data[] = $row;
        }
        return $data;
    }

    function addTodayMCQ($chapter_id, $question, $option_a, $option_b, $option_c, $option_d, $answer, $explanation, $livedate)
    {
        $sql = "INSERT INTO question (chapter_id,question,option_a,option_b,option_c,option_d,answer,explanation,created_date)VALUES ('{$chapter_id}','{$question}','{$option_a}','{$option_b}','{$option_c}','{$option_d}','{$answer}','{$explanation}','{$livedate}')";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r) {
            $sql2 = "SELECT LAST_INSERT_ID() AS id";
            $r2 = mysqli_query($this->dbsel, $sql2);
            if ($r2) {
                $row = mysqli_fetch_assoc($r2);
                $questionId = $row['id'];
                return $questionId;
            } else {
                return '';
            }
        } else return '';
    }


    function updateQuestion($question_id, $chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation)
    {
        $trimmed_explanation = str_replace("'", '', $explanation);
        $sql = "UPDATE question SET question = '{$question}',option_a = '{$option1}',option_b = '{$option2}',option_c = '{$option3}',option_d = '{$option4}',answer = '{$answer}',explanation = '{$trimmed_explanation}' where id = '{$question_id}' && chapter_id='{$chapter_id}'";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r == true)
            return true;
        else
            return false;
    }

    function updateQuestionMCQ($question_id, $chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation, $livedate)
    {
        $trimmed_explanation = str_replace("'", '', $explanation);
        $sql = "UPDATE question SET question = '{$question}',option_a = '{$option1}',option_b = '{$option2}',option_c = '{$option3}',option_d = '{$option4}',answer = '{$answer}',explanation = '{$trimmed_explanation}',created_date = '{$livedate}' where id = '{$question_id}' && chapter_id='{$chapter_id}'";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r == true)
            return true;
        else
            return false;
    }


    function getQuestionImages($question_id)
    {
        $sql = "SELECT * from question_image where question_id ='{$question_id}'";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r)) {
            $data[] = $row;
        }
        return $data;
    }

    function deleteImageById($imageId)
    {
        $sql = "DELETE from question_image where id ='{$imageId}'";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r == true)
            return true;
        else
            return false;
    }

    function InsertQuestionImage($que_id, $filename, $tempname, $i)
    {
        $filenameext = pathinfo($filename, PATHINFO_EXTENSION); //filename only extension
        $name = date("mjYHis") . "_" . $i . "." . $filenameext;
        $image = "SynapseBackend/Images/question/" . $name;
        if (move_uploaded_file($tempname, $image)) {
            $sql1 = "INSERT INTO question_image (image,question_id) VALUES ('{$image}','{$que_id}')";
            $r1 = mysqli_query($this->dbsel, $sql1);
            if ($r1 == true)
                return true;
            else return false;
        } else
            return false;
    }

    function UpdateQuestionImage($que_image_id, $question_id, $filename, $tempname, $i)
    {
        $filenameext = pathinfo($filename, PATHINFO_EXTENSION); //filename only extension
        $name = date("mjYHis") . "_" . $i . "." . $filenameext;
        $image = "SynapseBackend/Images/question/" . $name;
        if (move_uploaded_file($tempname, $image)) {
            $sql1 = "UPDATE question_image SET image = '{$image}' where id = '{$que_image_id}' and question_id='{$question_id}'";
            $r1 = mysqli_query($this->dbsel, $sql1);
            if ($r1 == true)
                return true;
            else return false;
        } else
            return false;
    }
}
