<?php
class SubjectService
{


    public $dbsel;

    function __construct($db)
    {
        $this->dbsel = $db;
    }

    function getSubjects($topicId)
    {
        $query = "SELECT s.id,s.name,s.topic_id,s.photourl, (SELECT COUNT(*) from chapter c Where c.topic_id='{$topicId}' && c.subject_id=s.id)AS no_of_chapters FROM subject s where s.topic_id ='{$topicId}' order by name asc";
        $r =  mysqli_query($this->dbsel, $query);
        $data = array();
        while ($row = mysqli_fetch_assoc($r))
            $data[] = $row;

        return $data;
    }


    function getSubjectName($topicId, $subjectId)
    {
        $query = "SELECT * FROM subject WHERE topic_id=$topicId and id=$subjectId";
        $r =  mysqli_query($this->dbsel, $query);
        // $data = array();
        // while (
        $row = mysqli_fetch_array($r);
        // $data[] = $row;

        $name = $row[1];
        return $name;
    }


    // function getChapterByTopic($topicId)
    // {
    //     $sql = "SELECT * from subject where topic_id = $topicId order by name asc";
    //     $r = mysqli_query($this->dbsel, $sql);
    //     $data = array();
    //     while ($row = mysqli_fetch_assoc($r))
    //         $data[] = $row;

    //     return $data;
    // }

    function addSubjects($topic_id, $subjectname, $filename, $tempname, $folder)
    {
        $sql = "INSERT INTO subject (name,topic_id,photourl)VALUES ('{$subjectname}','{$topic_id}','{$folder}')";
        $r = mysqli_query($this->dbsel, $sql);

        if (move_uploaded_file($tempname, $folder) && $r) {
            return true;
        } else {
            return false;
        }
    }

    function updateSubjects($subject_id, $topic_id, $subjectname, $filename, $tempname, $folder)
    {

        $query = "SELECT photourl FROM subject WHERE id=$subject_id";
        $result =  mysqli_query($this->dbsel, $query);
        if ($result) {
            $row = mysqli_fetch_array($result);
            $previousfile = $row['photourl'];
            $path =  $previousfile;
            $imageQuery = $folder == '' ? "" : ",photourl='{$folder}'";
            $sql = "UPDATE subject SET name='{$subjectname}' {$imageQuery} where id='{$subject_id}'";
            $r = mysqli_query($this->dbsel, $sql);
            if ($folder != '') {
                if (unlink($path) && move_uploaded_file($tempname, $folder) && $r) {
                    return true;
                } else {
                    return false;
                }
            }
            return $r;
        } else {
            return false;
        }
    }
}
