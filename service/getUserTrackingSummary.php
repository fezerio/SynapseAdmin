<?php
include('../security.php');

$sql = "SELECT (Select Count(*) from question_tracking qt1 where qt1.user_answer!=0) AS attempted_question,(Select Count(*) from question_tracking qt1 where qt1.user_answer=0) AS left_question, SUM(CASE WHEN qt.correct_answer=qt.user_answer THEN 1 ELSE 0 END) AS correct_answer, SUM(CASE WHEN qt.correct_answer != qt.user_answer AND qt.user_answer!=0 THEN 1 ELSE 0 END) AS incorrect_answer from question_tracking qt ";
$r = mysqli_query($connection, $sql);
$row = mysqli_fetch_assoc($r);
echo json_encode($row);
