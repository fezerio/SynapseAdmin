<?php
// // include('service/dbconnect.php');
// include('../security.php');

// $subjectservice = new ChapterService($connection);
// $date = new DateTime('2020-12-10');
// $data = $subjectservice->addChapter(2, 4, 'new ch1apter', 33, $date->format('Y-m-d H:i:s'));
class ChapterService
{

    public $dbsel;

    function __construct($db)
    {
        $this->dbsel = $db;
    }

    function getChapter($topicId, $subjectId)
    {
        $sql = "SELECT c.id,c.name,c.topic_id,c.subject_id,c.created_date,c.live_date, c.timer,(Select COUNT(*) FROM question q where q.chapter_id =c.id) AS no_of_questions from chapter c  where c.topic_id = $topicId && c.subject_id =$subjectId ORDER BY live_date desc";
        // $sql = "SELECT * FROM chapter WHERE subject_id = '{$subjectId}' && topic_id = $topicId order by name asc";
        $r = mysqli_query($this->dbsel, $sql);

        $data = array();
        while ($row = mysqli_fetch_assoc($r))
            $data[] = $row;
        return $data;
    }

    function addChapter($name, $topic_id, $subject_id, $timer, $liveDate)
    {
        $sql = "INSERT INTO chapter (name,topic_id,subject_id,timer,no_of_questions,live_date)VALUES ('{$name}','{$topic_id}','{$subject_id}','{$timer}',0,'{$liveDate}')";
        $r = mysqli_query($this->dbsel, $sql);
        echo ($r);
        if ($r == true)
            return true;
        return false;
    }

    function getChapterName($chapterId)
    {
        $query = "SELECT * FROM chapter WHERE id=$chapterId";
        $r =  mysqli_query($this->dbsel, $query);
        // $data = array();
        // while (
        $row = mysqli_fetch_array($r);
        // $data[] = $row;

        $name = $row[1];
        return $name;
    }

    function getTotalChapterCount()
    {
        $sql = "SELECT COUNT(*) AS count from chapter ";
        $r = mysqli_query($this->dbsel, $sql);
        $row = mysqli_fetch_assoc($r);
        return $row['count'];
    }


    function UpdateChapter($chapter_id, $name, $topic_id, $subject_id, $timer, $live_date)
    {
        $sql = "UPDATE chapter SET name = '{$name}',live_date = '{$live_date}',timer = '{$timer}' where id = '{$chapter_id}' && topic_id='{$topic_id}' && subject_id ='{$subject_id}'";
        $r = mysqli_query($this->dbsel, $sql);
        if ($r == true)
            return true;
        else
            return false;
    }
}

if (isset($_POST['subject_id'])) {
    $topicId = $_POST['topic_id'];
    $sub = $_POST['subject_id'];
    $query = "SELECT * FROM chapter WHERE subject_id =  '{$sub}' && topic_id = $topicId order by name asc";
    $results =  mysqli_query($connection, $query);
    $resultset = array();
    while ($row = mysqli_fetch_assoc($results)) {
        $resultset[] = $row;
    }
    echo json_encode($resultset);
}
