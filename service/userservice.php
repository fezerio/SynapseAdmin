<?php
// include('../security.php');
// $userService = new UserService($connection);
// $userService->getQuestionSummary();
// $query = "SELECT id FROM user ORDER BY id";
// $query_run = mysqli_query($connection, $query);
// $row = mysqli_num_rows($query_run);
class UserService
{
    public $dbsel;

    function __construct($db)
    {
        $this->dbsel = $db;
    }

    function getTotalUser()
    {
        $sql = "SELECT Count(*) AS count from user where provider!='admin'";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        $row = mysqli_fetch_assoc($r);
        return $row['count'];
    }

    function getTotalAdmin()
    {
        $sql = "SELECT Count(*) AS count from user where provider='admin'";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        $row = mysqli_fetch_assoc($r);
        return $row['count'];
    }

    function getAdminData()
    {
        $sql = "SELECT * from user where provider='admin'";
        $r = mysqli_query($this->dbsel, $sql);
        return $r;
    }

    function deleteAdmin($user_id)
    {
        $sql = "DELETE from user where firebase_uid = '{$user_id}' && provider='admin' ";
        $r = mysqli_query($this->dbsel, $sql);
        return $r;
    }

    function getUserData()
    {
        $sql = "SELECT email_id,photo_url,name,provider,created_date from user where provider!='admin'";
        $r = mysqli_query($this->dbsel, $sql);
        return $r;
    }



    // function getQuestionSummary()
    // {

    // }


    // function getQuestionSummaryByMonth()
    // {
    // }
}
