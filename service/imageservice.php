<?php

class ImageService
{
    function __construct()
    {
        
    }

	function checkImageConditions($filename, $filesize)
    {
		$allowed_ext = array("jpeg", "jpg", "png");
        $tmp = explode('.', $filename);
        $ext = end($tmp);
        if (in_array($ext, $allowed_ext)) 
        {
            if ($filesize < 300000) 
            {
                return true;
            } else {
                return false;
            }
        } else {
           return false;
        }
   
    }

}