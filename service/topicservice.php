<?php
// include('service/dbconnect.php');

class TopicService
{

    public $dbsel;

    function __construct($db)
    {
        $this->dbsel = $db;
    }

    function getTopic($topicId)
    {
        $sql = "SELECT * from topic where id = {$topicId}";
        $r = mysqli_query($this->dbsel, $sql);
        $data = array();
        while ($row = mysqli_fetch_assoc($r))
            $data[] = $row;

        return $data;
    }
}
