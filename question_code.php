<?php
include('security.php');
include('service/questionservice.php');
// include('service/imageservice.php');
$imageservice = new ImageService();
$questionservice = new QuestionService($connection);
$chapter_id = $_GET['chapter_id'];

if (isset($_POST['questionaddbtn'])) {


	$question = $_POST['enter_question'];

	// For que image part
	$question_image1 = $_FILES["question_image1"]["name"];
	$question_image1 = preg_replace("/\s+/", "_", $question_image1); //replacing space with underscore in name 
	$question_image1_tempname = $_FILES["question_image1"]["tmp_name"];
	$question_image1_size = $_FILES["question_image1"]["size"];

	$question_image2 = $_FILES["question_image2"]["name"];
	$question_image2 = preg_replace("/\s+/", "_", $question_image2); //replacing space with underscore in name 
	$question_image2_tempname = $_FILES["question_image2"]["tmp_name"];
	$question_image2_size = $_FILES["question_image2"]["size"];

	$question_image3 = $_FILES["question_image3"]["name"];
	$question_image3 = preg_replace("/\s+/", "_", $question_image3); //replacing space with underscore in name 
	$question_image3_tempname = $_FILES["question_image3"]["tmp_name"];
	$question_image3_size = $_FILES["question_image3"]["size"];



	$option1 = $_POST['option_1'];
	$option2 = $_POST['option_2'];
	$option3 = $_POST['option_3'];
	$option4 = $_POST['option_4'];
	$answer = $_POST['answer'];
	$explanation = $_POST['quill_html'];


	if ($chapter_id == 0) {
		$chapter_name = $_POST['chapter'];
		$live_date = new DateTime($_POST['livedate']);
		$date = $live_date->format('Y-m-d');
		$que_id = $questionservice->addTodayMCQ($chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation, $date);

		if ($que_id != '' && $question_image1 != '' && $imageservice->checkImageConditions($question_image1, $question_image1_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image1, $question_image1_tempname, 1);
		}
		if ($que_id != '' && $question_image2 != '' && $imageservice->checkImageConditions($question_image2, $question_image2_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image2, $question_image2_tempname, 2);
		}
		if ($que_id != '' && $question_image3 != '' && $imageservice->checkImageConditions($question_image3, $question_image3_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image3, $question_image3_tempname, 3);
		}
	} else {

		$que_id = $questionservice->insertQuestion($chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation);

		if ($que_id != '' && $question_image1 != '' && $imageservice->checkImageConditions($question_image1, $question_image1_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image1, $question_image1_tempname, 1);
		}
		if ($que_id != '' && $question_image2 != '' && $imageservice->checkImageConditions($question_image2, $question_image2_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image2, $question_image2_tempname, 2);
		}
		if ($que_id != '' && $question_image3 != '' && $imageservice->checkImageConditions($question_image3, $question_image3_size)) {
			$r = $questionservice->InsertQuestionImage($que_id, $question_image3, $question_image3_tempname, 3);
		}
	}

	if ($que_id != '') {
		$_SESSION['status'] = "Question has been added!";
		header('Location: questions.php?chapter_id=' . $chapter_id . '');
	} else {
		$_SESSION['status'] = "Error adding questions";
		header('Location: questions.php?chapter_id=' . $chapter_id . '');
	}
}


if (isset($_POST['questioneditbtn'])) {

	$question_id = $_POST['question_id'];
	$question = $_POST['enter_question'];

	// For que image part
	$question_image1 = $_FILES["question_image1"]["name"];
	$question_image1 = preg_replace("/\s+/", "_", $question_image1); //replacing space with underscore in name 
	$question_image1_tempname = $_FILES["question_image1"]["tmp_name"];
	$question_image1_size = $_FILES["question_image1"]["size"];
	$question_image1_id = $_POST['question_image1_id'];

	$question_image2 = $_FILES["question_image2"]["name"];
	$question_image2 = preg_replace("/\s+/", "_", $question_image2); //replacing space with underscore in name 
	$question_image2_tempname = $_FILES["question_image2"]["tmp_name"];
	$question_image2_size = $_FILES["question_image2"]["size"];
	$question_image2_id = $_POST['question_image2_id'];

	$question_image3 = $_FILES["question_image3"]["name"];
	$question_image3 = preg_replace("/\s+/", "_", $question_image3); //replacing space with underscore in name 
	$question_image3_tempname = $_FILES["question_image3"]["tmp_name"];
	$question_image3_size = $_FILES["question_image3"]["size"];
	$question_image3_id = $_POST['question_image3_id'];

	$option1 = $_POST['option_1'];
	$option2 = $_POST['option_2'];
	$option3 = $_POST['option_3'];
	$option4 = $_POST['option_4'];
	$answer = $_POST['answer'];
	$explanation = $_POST['quill1_html'];
	// echo ($explanation);
	// return;
	if ($chapter_id == 0) {
		$chapter_name = $_POST['chapter'];
		$live_date = new DateTime($_POST['livedate']);
		$date = $live_date->format('Y-m-d');
		$result = $questionservice->updateQuestionMCQ($question_id, $chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation, $date);

		if ($result && $question_image1 != '' && $imageservice->checkImageConditions($question_image1, $question_image1_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image1_id, $question_id, $question_image1, $question_image1_tempname, 1);
		}
		if ($result && $question_image2 != '' && $imageservice->checkImageConditions($question_image2, $question_image2_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image2_id, $question_id, $question_image2, $question_image2_tempname, 2);
		}
		if ($result && $question_image3 != '' && $imageservice->checkImageConditions($question_image3, $question_image3_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image3_id, $question_id, $question_image3, $question_image3_tempname, 3);
		}
	} else {
		$result = $questionservice->updateQuestion($question_id, $chapter_id, $question, $option1, $option2, $option3, $option4, $answer, $explanation);

		if ($result && $question_image1 != '' && $imageservice->checkImageConditions($question_image1, $question_image1_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image1_id, $question_id, $question_image1, $question_image1_tempname, 1);
		}
		if ($result && $question_image2 != '' && $imageservice->checkImageConditions($question_image2, $question_image2_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image2_id, $question_id, $question_image2, $question_image2_tempname, 2);
		}
		if ($result && $question_image3 != '' && $imageservice->checkImageConditions($question_image3, $question_image3_size)) {
			$r = $questionservice->UpdateQuestionImage($question_image3_id, $question_id, $question_image3, $question_image3_tempname, 3);
		}
	}


	if ($result) {
		$_SESSION['status'] = "Update Sucess!";
		header('Location: questions.php?chapter_id=' . $chapter_id . '');
	} else {
		$_SESSION['status'] = "Error Updating question!";
		header('Location: questions.php?chapter_id=' . $chapter_id . '');
	}
}
