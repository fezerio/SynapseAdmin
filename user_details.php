<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
include('service/userservice.php');

?>

<div class="container-fluid">

    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">User Details</h6>
        </div>

        <div class="card-body">

            <?php
            if (isset($_SESSION['success']) && $_SESSION['success'] != '') {
                echo '<h2>' . $_SESSION['success'] . '</h2>';
                unset($_SESSION['success']);
            }
            if (isset($_SESSION['status']) && $_SESSION['status'] != '') {
                echo '<h2>' . $_SESSION['status'] . '</h2>';
                unset($_SESSION['status']);
            }
            ?>

            <div class="table-responsive">
                <?php
                $userservice = new UserService($connection);
                $query_run = $userservice->getUserData();
                ?>
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th> S.No</th>
                            <th> Username </th>
                            <th> Email_id</th>
                            <th> Login Method</th>
                            <th>Joined Date</th>
                            <th> Photo </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (mysqli_num_rows($query_run) > 0) {
                            $z = 0;
                            while ($row = mysqli_fetch_assoc($query_run)) {
                                $z++;
                        ?>
                                <tr>
                                    <td><?php echo  $z; ?></td>
                                    <td><?php echo $row['name']; ?></td>
                                    <td><?php echo $row['email_id']; ?></td>
                                    <td><?php echo strtoupper($row['provider'] == 'firebase' ? 'Email' : $row['provider']); ?></td>
                                    <td><?php $date = new DateTime($row['created_date']);
                                        echo $date->format('D M d, Y');
                                        ?></td>
                                    <?php
                                    if (!($row['photo_url'] == null || ((string) $row['photo_url']) == '')) {
                                    ?>
                                        <td><img id="user_img_tag" src="<?php echo  $row['photo_url'] == null ? '' : ($row['photo_url'] == '' ? '' : "SynapseBackend/" . $row['photo_url']); ?>" alt="subject Image" height="60px" width="60px"></td>
                                    <?php
                                    } else {
                                    ?>
                                        <td><?php echo 'No Image' ?></td>
                                    <?php
                                    }
                                    ?>
                                </tr>
                        <?php
                            }
                        } else {
                            echo "No Record Found";
                        }
                        ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

</div>


<?php

include('includes/scripts.php');
?>