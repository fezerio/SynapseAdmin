<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
?>

<div class="container-fluid">

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary"> EDIT Admin Profile </h6>
        </div>
        <div class="card-body">
            <?php

            if (isset($_POST['edit_btn'])) {
                $user_id = $_POST['firebase_uid'];
                $email_id = $_POST['email_id'];

                // $query = "SELECT * FROM admin WHERE id='$id' ";
                // $query_run = mysqli_query($connection, $query);

                // foreach($query_run as $row)
                // {
            ?>

                <form action="register_admin_code.php" method="POST">

                    <input type="hidden" name="edit_id" value="<?php echo $user_id ?>">

                    <div class="form-group">
                        <label> Username </label>
                        <input type="text" disabled="disabled" name="userName" value="<?php echo $user_id ?>" class="form-control" placeholder="Enter Username">
                    </div>
                    <div class="form-group">
                        <label>Email</label>
                        <input type="email" disabled="disabled" name="edit_email" value="<?php echo $email_id ?>" class="form-control" placeholder="Enter Email">
                    </div>
                    <div class="form-group">
                        <label>Old Password</label>
                        <input type="password" name="old_password" class="form-control" placeholder="Enter Old Password">
                    </div>
                    <div class="form-group">
                        <label>New Password</label>
                        <input type="password" name="new_password" class="form-control" placeholder="Enter New Password">
                    </div>

                    <a href="register_admin.php" class="btn btn-danger"> CANCEL </a>
                    <button type="submit" name="updatebtn" class="btn btn-primary"> Update </button>

                </form>
            <?php
                // }
            }
            ?>
        </div>
    </div>
</div>

</div>

<?php

include('includes/scripts.php');
?>