<?php
include('security.php');
include('service/authentication.php');
include('service/userservice.php');

if (isset($_POST['registerbtn'])) {
    $username = $_POST['username'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $cpassword = $_POST['confirmpassword'];

    $adminAuthentication = new Authentication($username, $password, $connection);

    // $email_query = "SELECT * FROM admin WHERE email='$email' ";
    // $email_query_run = mysqli_query($connection, $email_query);

    // if (mysqli_num_rows($email_query_run) > 0) {
    //     $_SESSION['status'] = "Email Already Taken. Please Try Another one.";
    //     $_SESSION['status_code'] = "error";
    //     header('Location: register_admin.php');
    // } else {
    if ($password == $cpassword) {
        $result = $adminAuthentication->signUpAdmin('Admin', $email);
        if ($result == null) {
            $_SESSION['status'] = "Problem Creating Admin Account";
            $_SESSION['status_code'] = "error";
            header('Location: register_admin.php');
        }
        // $query = "INSERT INTO admin (name,email,password) VALUES ('$username','$email','$password')";
        // $query_run = mysqli_query($connection, $query);
        else {
            if ($result) {
                // echo "Saved";
                $_SESSION['status'] = "Admin Profile Added";
                $_SESSION['status_code'] = "success";
                header('Location: register_admin.php');
            } else {
                $_SESSION['status'] = "Username Already Taken. Please Try Another one.";
                $_SESSION['status_code'] = "error";
                header('Location: register_admin.php');
            }
        }
    } else {
        $_SESSION['status'] = "Password and Confirm Password Does Not Match";
        $_SESSION['status_code'] = "warning";
        header('Location: register_admin.php');
    }
}



if (isset($_POST['updatebtn'])) {
    $userName = $_POST['edit_id'];
    $oldpassword = $_POST['old_password'];
    $newpassword = $_POST['new_password'];
    $user = new Authentication($userName, $oldpassword, $connection);
    $query_run = $user->updatePassword($newpassword);
    if ($query_run == null) {
        $_SESSION['status'] = "Incorrect Old Password";
        $_SESSION['status_code'] = "error";
        header('Location: register_admin.php');
    } else if ($query_run) {
        $_SESSION['status'] = "Your Password is Updated";
        $_SESSION['status_code'] = "success";
        header('Location: register_admin.php');
    } else {
        $_SESSION['status'] = "Error Changins Password";
        $_SESSION['status_code'] = "error";
        header('Location: register_admin.php');
    }
}



if (isset($_POST['delete_btn'])) {
    $userId = $_POST['delete_id'];
    if ($userId == $_SESSION['user_id']) {
        $_SESSION['status'] = 'Cannot Delete this admin';
        $_SESSION['status_code'] = "error";
        header('Location: register_admin.php');
    } else {
        $userService = new UserService($connection);
        $query_run = $userService->deleteAdmin($userId);

        if ($query_run) {
            $_SESSION['status'] = "Your Data is Deleted";
            $_SESSION['status_code'] = "success";
            header('Location: register_admin.php');
        } else {
            $_SESSION['status'] = "Your Data is NOT DELETED";
            $_SESSION['status_code'] = "error";
            header('Location: register_admin.php');
        }
    }
}
