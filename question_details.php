<?php
include('security.php');
include('includes/header.php');
include('includes/navbar.php');
require 'service/questionservice.php';
$questionservice = new QuestionService($connection);
$chapter_id = $_GET['chapter_id'];
?>

<script type="text/javascript">
function triggerClick1(){
  document.querySelector('#question_image1').click();
}
function triggerClick2(){
  document.querySelector('#question_image2').click();
}
function triggerClick3(){
  document.querySelector('#question_image3').click();
}

function displayImage1(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay1').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function displayImage2(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay2').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
function displayImage3(e){
  if(e.files[0]){
    var reader = new FileReader();
    reader.onload = function(e){
      document.querySelector('#imageUploadDisplay3').setAttribute('src', e.target.result);
    }
    reader.readAsDataURL(e.files[0]);
  }
}
</script>

<script>
  function enableDisableAll(e) {
    var own = e;
    var form = document.getElementById("questioneditform");
    var elements = form.elements;
    var elementslength = elements.length;

    for (var i = 0; i < elements.length; i++) {
      if (own !== elements[i]) {
        if (elements[i] == elements[elementslength - 1] || elements[i] == elements[elementslength - 2]) {
          if (own.checked == true) {

            elements[i].hidden = false;
            quill1.enable(true);

          } else {
            elements[i].hidden = true;
            quill1.enable(false);
          }
        } else {
          if (own.checked == true) {

            elements[i].disabled = false;
            quill1.enable(true);

          } else {
            elements[i].disabled = true;
            quill1.enable(false);
          }
        }

      }
    }

  }
</script>

<div class="container-fluid">

  <!-- DataTales Example -->
  <div class="card shadow mb-4">
    <div class="card-header py-3">
      <h6 class="m-0 font-weight-bold text-primary"> Question Details </h6>
    </div>
    <div class="card-body">
      <?php

      if (isset($_POST['details_btn'])) {
        $questionId = $_POST['question_id'];
        $isReported = $_POST['isReported'];
        $question = $questionservice->getQuestionByQuestionId($questionId);
        $que_img = $questionservice->getQuestionImages($questionId);
        // $img = array();
        // foreach ($que_img as $rr) {
        //   $img[] = $rr['image'];
        // }
        foreach ($question as $r) {
      ?>

          <form id="questioneditform" action="question_code.php?chapter_id=<?php echo $chapter_id; ?>" method="POST" enctype="multipart/form-data">
            <div class="modal-body">

              <div class="form-group">
                <input type="text" name="question_id" id="question_id" class="form-control" value="<?php echo $r['id'] ?>" hidden>
              </div>

              <div class="form-group">
                <label style="color:black"><b>Question:</b></label>
                <input type="text" name="enter_question" id="enter_question" class="form-control" value="<?php echo $r['question'] ?>" disabled>
              </div>
              
                <div class="form-group" >
                  <label style="color:black"><b>Update images for the question:</b></label><br>
                  <?php
                    $img = array();
                    foreach ($que_img as $row) {
                      $img_id[] = $row['id'];
                      $img[] = $row['image'];
                    }
                  ?>
                  <img src="<?php echo $img[0] ?>" onerror="this.src='SynapseBackend/Images/question/placeholder.png';" onclick="triggerClick1()" id="imageUploadDisplay1" name="imageUploadDisplay1" height="100px" width="100px" disabled>
                  <input type="file" name="question_image1" id="question_image1" class="form-control" value="<?php echo $img[0] ?>" onchange="displayImage1(this)" style="display:none" disabled>
                  <input type="text" name="question_image1_id" id="question_image1_id" class="form-control" value="<?php echo $img_id[0] ?>" hidden>


                  <img src="<?php echo $img[1] ?>" onerror="this.src='SynapseBackend/Images/question/placeholder.png';" onclick="triggerClick2()" id="imageUploadDisplay2" name="imageUploadDisplay2" height="100px" width="100px" disabled>
                  <input type="file" name="question_image2" id="question_image2" class="form-control" value="<?php echo $img[1] ?>" onchange="displayImage2(this)" style="display:none" disabled>
                  <input type="text" name="question_image2_id" id="question_image2_id" class="form-control" value="<?php echo $img_id[1] ?>" hidden>

                  <img src="<?php echo $img[2] ?>" onerror="this.src='SynapseBackend/Images/question/placeholder.png';" onclick="triggerClick3()" id="imageUploadDisplay3" name="imageUploadDisplay3" height="100px" width="100px" disabled>
                  <input type="file" name="question_image3" id="question_image3" class="form-control" value="<?php echo $img[2] ?>" onchange="displayImage3(this)" style="display:none" disabled>
                  <input type="text" name="question_image3_id" id="question_image3_id" class="form-control" value="<?php echo $img_id[2] ?>" hidden>
                
                </div>
              

              <div class="form-group">
                <label style="color:black"><b>Option 1:</b></label>
                <input type="text" name="option_1" id="option_1" class="form-control" value="<?php echo $r['option_a'] ?>" disabled>
              </div>
              <div class="form-group">
                <label style="color:black"><b>Option 2:</b></label>
                <input type="text" name="option_2" id="option_2" class="form-control" value="<?php echo $r['option_b'] ?>" disabled>
              </div>
              <div class="form-group">
                <label style="color:black"><b>Option 3:</b></label>
                <input type="text" name="option_3" id="option_3" class="form-control" value="<?php echo $r['option_b'] ?>" disabled>
              </div>
              <div class="form-group">
                <label style="color:black"><b>Option 4:</b></label>
                <input type="text" name="option_4" id="option_4" class="form-control" value="<?php echo $r['option_b'] ?>" disabled>
              </div>
              <div class="form-group">
                <label style="color:black"><b>Answer:</b></label>
                <select name="answer" disabled>
                  <option value="1" <?php if ($r['answer'] == 1) { ?> selected <?php } ?>>Option 1</option>
                  <option value="2" <?php if ($r['answer'] == 2) { ?> selected <?php } ?>>Option 2</option>
                  <option value="3" <?php if ($r['answer'] == 3) { ?> selected <?php } ?>>Option 3</option>
                  <option value="4" <?php if ($r['answer'] == 4) { ?> selected <?php } ?>>Option 4</option>
                </select>
              </div>

               <div class="form-group">
                  <label style="color:black"><b>Explanation:</b></label>
                  <div id="explanation_editor_edit" style="max-height:500px;overflow:auto"></div>
                  <input type="hidden" name="quill1_html" id="quill1_html" class="form-control" value='<?php echo $r['explanation'] ?>' >
                </div>


              <?php if ($chapter_id == 0) {
              ?>
                <div class="form-group">
                  <label> Enter Live date: </label>
                  <input type="date" name="livedate" class="form-control" value="<?php echo $r['created_date'] ?>" disabled>
                </div>
              <?php
              }
              ?>
              <?php if ($isReported != true) {
              ?>
                <div class="form-group">
                  <label for="editcheckbox"> Edit details</label>
                  <input type="checkbox" id="editcheckbox" name="editcheckbox" onchange="enableDisableAll(this)">
                </div>
            </div>
          <?php
              }
          ?>
          <div class="modal-footer">
            <button type="button" name="closebtn" class="btn btn-secondary" data-dismiss="modal" value="close" hidden>Close</button>
            <button type="submit" id="questioneditbtn" name="questioneditbtn" class="btn btn-primary" value="edit" hidden>Edit</button>
          </div>
      <?php
        }
      }
      ?>
    </div>
  </div>
</div>

</div>

<?php

include('includes/scripts.php');
?>