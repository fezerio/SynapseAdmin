<?php
include('security.php');
include('service/subjectservice.php');
$topic_id = $_GET['topic_id'];
$topic_name = $_GET['topic_name'];
$subjectservice = new SubjectService($connection);

if (isset($_POST['addbtn'])) {
    $subjectname = $_POST['subject'];
    $filename = $_FILES["subject_image"]["name"];
    $filename = preg_replace("/\s+/", "_", $filename); //replacing space with underscore in name 
    $tempname = $_FILES["subject_image"]["tmp_name"];
    if ($filename != '') { //checking if file exist
        $allowed_ext = array("jpeg", "jpg", "png");
        $ext = end(explode('.', $_FILES["subject_image"]["name"]));
        if (in_array($ext, $allowed_ext)) {
            if ($_FILES["subject_image"]["size"] < 300000) {
                $filenamewext = pathinfo($filename, PATHINFO_FILENAME); //filename without extension
                $filenameext = pathinfo($filename, PATHINFO_EXTENSION); //filename only extension
                $name = $subjectname . "_" . date("mjYHis") . "." . $filenameext;
                $folder = "SynapseBackend/Images/subject/" . $name;
                $result = $subjectservice->addSubjects($topic_id, $subjectname, $name, $tempname, $folder);
                if ($result) {
                    // $_SESSION['success'] = $subjectname . " added!";
                    header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
                } else {
                    $_SESSION['status'] = "Error adding Subject!";
                    header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
                }
            } else {
                $_SESSION['status'] = "Big Image File.";
                header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
            }
        } else {
            $_SESSION['status'] = "Invalid Image File.";
            header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
        }
    } else {

        $_SESSION['status'] = "Please select a file";
        header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
    }
}


if (isset($_POST['updatebtn'])) {

    $subject_id = $_POST['subject_id'];
    $subjectname = $_POST['subject_name'];
    $filename = $_FILES["subject_image"]["name"];
    $filename = preg_replace("/\s+/", "_", $filename); //replacing space with underscore in name 
    $tempname = $_FILES["subject_image"]["tmp_name"];
    // $folder = "Images/subject/".$filename; 

    //checking if file exist
    if ($filename != '') {
        $allowed_ext = array("jpeg", "jpg", "png");
        $ext = end(explode('.', $_FILES["subject_image"]["name"]));
        if (in_array($ext, $allowed_ext)) {
            if ($_FILES["subject_image"]["size"] < 300000) {
                $filenamewext = pathinfo($filename, PATHINFO_FILENAME); //filename without extension
                $filenameext = pathinfo($filename, PATHINFO_EXTENSION); //filename only extension
                $name = $subjectname . "_" . date("mjYHis") . "." . $filenameext;
                $folder = "SynapseBackend/Images/subject/" . $name;
                $result = $subjectservice->updateSubjects($subject_id, $topic_id, $subjectname, $name, $tempname, $folder);
                if ($result) {
                    header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
                } else {
                    $_SESSION['status'] = "Error updating Subject!";
                    header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
                }
            } else {
                $_SESSION['status'] = "Big Image File.";
                header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
            }
        } else {
            $_SESSION['status'] = "Invalid Image File.";
            header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
        }
    } else {
        $result = $subjectservice->updateSubjects($subject_id, $topic_id, $subjectname, '', '', '');
        if ($result) {
            header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
        } else {
            $_SESSION['status'] = "Error updating Subject!";
            header('Location: subjects.php?topic_id=' . $topic_id . '&topic_name=' . $topic_name . '');
        }
    }
}
